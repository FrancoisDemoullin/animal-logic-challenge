CFLAGS=-I./src/ -I./include/3rd_party/ -I./include/3rd_party/glm/ -I./src/BVH/
DEBUGFLAGS = # -g -G # use these flags for debugging only - never for measuring performance
EXECNAME=bin/run
VERSION=-std=c++11
CPUPARALLEL= -Xcompiler -fopenmp
OPTIMIZATION = -O3

all: compile link

compile:
	nvcc -c src/main.cpp src/unit_test.cpp src/BVH/bb.cpp src/BVH/bvh.cpp $(VERSION) $(CFLAGS) $(DEBUGFLAGS) $(CPUPARALLEL) $(OPTIMIZATION)
	nvcc -c src/kernels.cu $(VERSION) $(CFLAGS) $(DEBUGFLAGS) $(OPTIMIZATION)

link:
	nvcc main.o unit_test.o kernels.o bvh.o bb.o $(CPUPARALLEL) -o $(EXECNAME) $(DEBUGFLAGS)
	
clean:
	rm main.o unit_test.o kernels.o bvh.o bb.o bin/*