#pragma once
#include <iostream>
#include <vector>

/*
Abstract Mesh class
Interface for any mesh classes (e.g. TriangleMesh, QuadMesh, etc.)
*/
template <class T>
class mesh {
  public:
  mesh(){};
  ~mesh(){};

  virtual void load_data_ply(const std::string file_name) = 0;

  virtual void closest_pt_on_mesh_brute_force(const glm::vec3 query_pt,
      const float min_radius,
      glm::vec3& closest_pt,
      float& dist) const = 0;

  virtual void closest_pt_on_mesh_brute_force_cpu_parallel(const glm::vec3 query_pt,
      const float min_radius,
      glm::vec3& closest_pt,
      float& dist) const = 0;

  virtual void closest_pt_on_mesh_brute_force_gpu(const glm::vec3 query_pt,
      const float min_radius,
      glm::vec3& closest_pt,
      float& dist) const = 0;
};