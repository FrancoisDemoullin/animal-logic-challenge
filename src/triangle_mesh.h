#pragma once
#include "BVH/bvh.h"
#include "kd_tree_manager.h"
#include <glm/vec3.hpp>
#include <iostream>
#include <mesh.h>
#include <set>
#include <unordered_map>
#include <utility>
#include <vector>

/*
 * Triangle Mesh class
 * Implements the abstract mesh class
*/
template <class T>
class triangle_mesh : public mesh<T> {
  public:
  triangle_mesh(){};

  ~triangle_mesh()
  {
    for (auto p : mesh_for_bvh)
      delete p;
    mesh_for_bvh.clear();
    connectivity_lookup_map.clear();
  };

  void load_data_ply(const std::string file_name);

  void closest_pt_on_mesh_brute_force(const glm::vec3 query_pt,
      const float min_radius,
      glm::vec3& closest_pt,
      float& dist) const;
  void closest_pt_on_mesh_brute_force_cpu_parallel(const glm::vec3 query_pt,
      const float min_radius,
      glm::vec3& closest_pt,
      float& dist) const;
  void closest_pt_on_mesh_brute_force_gpu(const glm::vec3 query_pt,
      const float min_radius,
      glm::vec3& closest_pt,
      float& dist) const;

  void closest_pt_on_mesh_brute_force_optimized(const glm::vec3 query_pt,
      const float min_radius,
      glm::vec3& closest_pt,
      float& dist) const;

  void closest_pt_on_mesh_vert_triangle_optimization(const glm::vec3 query_pt,
      const float min_radius,
      glm::vec3& closest_pt,
      float& dist) const;

  void closest_pt_kd_tree(const glm::vec3 query_pt,
      const float min_radius,
      glm::vec3& closest_pt,
      float& dist) const;

  void closest_pt_bvh(const glm::vec3 query_pt,
      const float min_radius,
      glm::vec3& closest_pt,
      float& dist) const;

  private:
  std::vector<T> v_x;
  std::vector<T> v_y;
  std::vector<T> v_z;
  std::vector<std::vector<int>> vertex_indices;
  std::unordered_map<int, std::set<std::pair<int, int>>> connectivity_lookup_map;

  kd_tree_manager kd_tree_instance;
  std::unique_ptr<BVH> bvh_instance;
  std::vector<triangle*> mesh_for_bvh;

  glm::vec3 bb_min;
  glm::vec3 bb_max;

  void update_hash_map(const std::pair<int, int>& a, const int& key);
  void init_hash_map();

  void pre_process_mesh();

  inline bool early_rejection_test(const glm::vec3& query_pt, const float& radius_squared) const
  {
    float sqDist = 0.0f;
    for (int i = 0; i < 3; i++) {
      float v = query_pt[i];
      if (v < bb_min[i])
        sqDist += (bb_min[i] - v) * (bb_min[i] - v);
      if (v > bb_max[i])
        sqDist += (v - bb_max[i]) * (v - bb_max[i]);
    }
    return (sqDist > radius_squared);
  }
};

#include "triangle_mesh.cpp"