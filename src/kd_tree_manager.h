#pragma once
#include <glm/vec3.hpp>
#include <memory>
#include <nanoflann.h>

using namespace std;
using namespace nanoflann;

// This is an exampleof a custom data set class
template <typename T>
struct PointCloud {
  struct Point {
    T x, y, z;
  };

  std::vector<Point> pts;

  // Must return the number of data points
  inline size_t kdtree_get_point_count() const { return pts.size(); }

  // Returns the dim'th component of the idx'th point in the class:
  // Since this is inlined and the "dim" argument is typically an immediate value, the
  //  "if/else's" are actually solved at compile time.
  inline T kdtree_get_pt(const size_t idx, const size_t dim) const
  {
    if (dim == 0)
      return pts[idx].x;
    else if (dim == 1)
      return pts[idx].y;
    else
      return pts[idx].z;
  }

  // Optional bounding-box computation: return false to default to a standard bbox computation loop.
  //   Return true if the BBOX was already computed by the class and returned in "bb" so it can be avoided to redo it again.
  //   Look at bb.size() to find out the expected dimensionality (e.g. 2 or 3 for point clouds)
  template <class BBOX>
  bool kdtree_get_bbox(BBOX& /* bb */) const { return false; }
};

typedef KDTreeSingleIndexAdaptor<
    L2_Simple_Adaptor<float, PointCloud<float>>,
    PointCloud<float>,
    3 /* dim */
    >
    my_kd_tree_t;

class kd_tree_manager {
  public:
  kd_tree_manager()
      : index(nullptr){};

  void init_tree(const std::vector<float>& v_x, const std::vector<float>& v_y, const std::vector<float>& v_z);
  void search_closest_vertex(const glm::vec3& query_pt_p, int& ret_index_p) const;

  private:
  PointCloud<float> cloud;
  std::shared_ptr<my_kd_tree_t> index;
};

#include <kd_tree_manager.cpp>