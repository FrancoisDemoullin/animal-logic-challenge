#include "helper_math.h" // CUDA vector library
#include <chrono> // timing
#include <iomanip>
#include <iostream>
#include <stdio.h>
#include <thrust/extrema.h> // minimum on GPU

#define THREADS_PER_BLOCK 128 // multiple of 2, no more than 256 (on GTX2080)
#define CUDA_THREADS 65536 // make sure this is divisible by THREADS_PER_BLOCK: 65536 32768 16384 8192 4096

// Error checking utility for CUDA
#define gpuErrchk(ans)                    \
  {                                       \
    gpuAssert((ans), __FILE__, __LINE__); \
  }
inline void gpuAssert(cudaError_t code, const char* file, int line, bool abort = true)
{
  if (code != cudaSuccess) {
    fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
    if (abort)
      exit(code);
  }
}

/*
* Device function for closest point on line segment
*/
__device__
    float3
    closes_pt_on_segment_gpu(const float3 v_0, const float3 v_1, const float3 query_pt)
{
  float3 v = v_1 - v_0;
  float3 w = query_pt - v_0;
  float c1 = dot(w, v);
  if (c1 < 0) {
    return v_0;
  }

  float c2 = dot(v, v);
  if (c2 <= c1) {
    return v_1; // vertex is closest
  }

  // point on the line between v_0 and v_1 is closest
  float b = c1 / c2;
  float3 result = v_0 + v * b;
  return result;
}

/*
* Device function for distance squared between 2 points
* Inline it because of it's shortness
*/
__device__ inline float distance_squared(const float3 v_0, const float3 v_1)
{

  return (v_1.x - v_0.x) * (v_1.x - v_0.x) + (v_1.y - v_0.y) * (v_1.y - v_0.y) + (v_1.z - v_0.z) * (v_1.z - v_0.z);
}

/*
* closest point brute force CUDA kernel
*/
__global__ void closest_pt(
    const float* d_x, const float* d_y, const float* d_z, const unsigned int* d_c,
    const unsigned int num_connectivity,
    const float q_pt_x, const float q_pt_y, const float q_pt_z, const float radius,
    float* d_result_distance, float* d_result_points, const int tris_per_thread)
{
  float min_dist = radius * radius;
  float3 min_pt;
  float3 query_pt = make_float3(q_pt_x, q_pt_y, q_pt_z);

  // compute id
  unsigned int my_id = blockIdx.x * blockDim.x + threadIdx.x;

  // out of bounds checking
  if (my_id >= CUDA_THREADS) {
    return;
  }

  unsigned int lower_index = my_id * tris_per_thread;
  unsigned int upper_index = lower_index + tris_per_thread;

  // iterate over all the faces assigned to this particular cuda thread
  for (int i = lower_index; i < min(upper_index, num_connectivity); i++) {
    float3 v_0 = make_float3(d_x[d_c[3 * i]], d_y[d_c[3 * i]], d_z[d_c[3 * i]]);
    float3 v_1 = make_float3(d_x[d_c[3 * i + 1]], d_y[d_c[3 * i + 1]], d_z[d_c[3 * i + 1]]);
    float3 v_2 = make_float3(d_x[d_c[3 * i + 2]], d_y[d_c[3 * i + 2]], d_z[d_c[3 * i + 2]]);

    float3 u = v_1 - v_0;
    float3 v = v_2 - v_0;
    float3 w = query_pt - v_0;
    float3 n = cross(u, v);
    float gamma = dot(cross(u, w), n) / dot(n, n);
    float beta = dot(cross(w, v), n) / dot(n, n);
    float alpha = 1.f - beta - gamma;

    float3 intersection_pt;
    float min_dist_pt;

    if (alpha >= 0.f && alpha <= 1.f && beta >= 0.f && beta <= 1.f && gamma >= 0.f && gamma <= 1.f) {
      intersection_pt = alpha * v_0 + beta * v_1 + gamma * v_2;
      min_dist_pt = distance_squared(intersection_pt, query_pt);
    } else {
      // simplified the code -> less branch statements more compute
      float3 p_0 = closes_pt_on_segment_gpu(v_0, v_1, query_pt);
      float3 p_1 = closes_pt_on_segment_gpu(v_1, v_2, query_pt);
      float3 p_2 = closes_pt_on_segment_gpu(v_0, v_2, query_pt);

      float d_0 = distance_squared(p_0, query_pt);
      float d_1 = distance_squared(p_1, query_pt);
      float d_2 = distance_squared(p_2, query_pt);

      intersection_pt = p_0;
      min_dist_pt = d_0;

      if (d_1 < d_0 && d_1 < d_2) {
        intersection_pt = p_1;
        min_dist_pt = d_1;
      } else if (d_2 < d_0 && d_2 < d_1) {
        intersection_pt = p_2;
        min_dist_pt = d_2;
      }
    }

    if (min_dist_pt < min_dist) {
      min_dist = min_dist_pt;
      min_pt = intersection_pt;
    }
  }

  // return the result
  if (min_dist < radius * radius) {
    d_result_points[3 * my_id] = min_pt.x;
    d_result_points[3 * my_id + 1] = min_pt.y;
    d_result_points[3 * my_id + 2] = min_pt.z;

    d_result_distance[my_id] = min_dist;
  } else {
    d_result_distance[my_id] = -1.f;
  }
}

/*
* driver function for the cuda kernel
* this function sets up the GPU buffers, launches the kernel and performs the map reduce to find the min
*/
void cuda_entry_point(const float* x_data, const float* y_data, const float* z_data, const unsigned int* connectivity,
    const unsigned int num_verts, const unsigned int num_connectivity,
    const float q_pt_x, const float q_pt_y, const float q_pt_z, const float radius,
    float& closest_pt_x, float& closest_pt_y, float& closest_pt_z,
    float& dist)
{
  // host + device pointers
  float *d_x, *d_y, *d_z;
  unsigned int* d_c;
  float *h_result_distance, *h_result_points, *d_result_distance, *d_result_points;

  // allocate host memory
  h_result_distance = (float*)malloc(CUDA_THREADS * sizeof(float));
  h_result_points = (float*)malloc(CUDA_THREADS * 3 * sizeof(float));

  // allocate device memory
  gpuErrchk(cudaMalloc(&d_x, num_verts * sizeof(float)));
  gpuErrchk(cudaMalloc(&d_y, num_verts * sizeof(float)));
  gpuErrchk(cudaMalloc(&d_z, num_verts * sizeof(float)));
  gpuErrchk(cudaMalloc(&d_c, 3 * num_connectivity * sizeof(unsigned int)));

  // allocate device result buffer
  gpuErrchk(cudaMalloc(&d_result_distance, CUDA_THREADS * sizeof(float)));
  gpuErrchk(cudaMalloc(&d_result_points, CUDA_THREADS * 3 * sizeof(float)));

  // copy data to GPU
  gpuErrchk(cudaMemcpy(d_x, x_data, num_verts * sizeof(float), cudaMemcpyHostToDevice));
  gpuErrchk(cudaMemcpy(d_y, y_data, num_verts * sizeof(float), cudaMemcpyHostToDevice));
  gpuErrchk(cudaMemcpy(d_z, z_data, num_verts * sizeof(float), cudaMemcpyHostToDevice));
  gpuErrchk(cudaMemcpy(d_c, connectivity, 3 * num_connectivity * sizeof(unsigned int), cudaMemcpyHostToDevice));

  // compute the kernel extent
  dim3 threadsPerBlock(THREADS_PER_BLOCK, 1, 1);
  dim3 blockDim(CUDA_THREADS / THREADS_PER_BLOCK, 1, 1);

  // compute the amount of triangles each thread has to compute
  int tris_per_thread = std::ceil(num_connectivity / CUDA_THREADS);

  // launch kernel
  // kernel populates d_result_distance and d_result_points arrays
  auto start_kernel = std::chrono::high_resolution_clock::now();
  closest_pt<<<blockDim, threadsPerBlock>>>(d_x, d_y, d_z, d_c, num_connectivity, q_pt_x, q_pt_y, q_pt_z, radius,
      d_result_distance, d_result_points, tris_per_thread);
  gpuErrchk(cudaDeviceSynchronize());

  // do a parallel map reduce to find the minimum value
  auto end_kernel_start_map_reduce = std::chrono::high_resolution_clock::now();
  thrust::device_vector<float> d_vec(d_result_distance, d_result_distance + CUDA_THREADS);
  thrust::device_vector<float>::iterator iter = thrust::min_element(d_vec.begin(), d_vec.end());
  auto end_map_reduce = std::chrono::high_resolution_clock::now();

  // get the result to the CPU
  unsigned int position = iter - d_vec.begin();
  // we can access the result distance directly like this
  // we can't forget to sqrt it because the GPU is working with distance squared
  dist = std::sqrt(*iter);

  // copy the result point to the CPU; we don't need to copy the whole vector, we just need to copy 3 floats:
  gpuErrchk(cudaMemcpy((void*)&closest_pt_x, d_result_points + position, sizeof(float), cudaMemcpyDeviceToHost));
  gpuErrchk(cudaMemcpy((void*)&closest_pt_y, d_result_points + position + 1, sizeof(float), cudaMemcpyDeviceToHost));
  gpuErrchk(cudaMemcpy((void*)&closest_pt_z, d_result_points + position + 2, sizeof(float), cudaMemcpyDeviceToHost));

  // freeing GPU mem
  gpuErrchk(cudaFree(d_x));
  gpuErrchk(cudaFree(d_y));
  gpuErrchk(cudaFree(d_z));
  gpuErrchk(cudaFree(d_c));
  gpuErrchk(cudaFree(d_result_points));
  gpuErrchk(cudaFree(d_result_distance));

  // free host mem
  free(h_result_points);
  free(h_result_distance);

  std::cout << "========== CUDA VERBOSE: ==========" << std::endl;
  std::cout << "tris_per_thread: " << tris_per_thread << std::endl;
  std::cout << "kernel time: " << std::chrono::duration<double, std::milli>(end_kernel_start_map_reduce - start_kernel).count() << std::endl;
  std::cout << "map reduce time: " << std::chrono::duration<double, std::milli>(end_map_reduce - end_kernel_start_map_reduce).count() << std::endl;
  std::cout << "========== End CUDA ==========" << std::endl;
}