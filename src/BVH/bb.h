#pragma once

#include <glm/gtx/extended_min_max.hpp>
#include <glm/vec3.hpp>

struct bbox {
  glm::vec3 min, max, extent;

  bbox() {}
  bbox(const glm::vec3& min, const glm::vec3& max);
  bbox(const glm::vec3& p);

  void expandToInclude(const glm::vec3& p);
  void expandToInclude(const bbox& b);
  uint32_t maxDimension() const;
  float surfaceArea() const;

  bool test_sphere_intersection(const glm::vec3& query_pt, const float radius) const;
};

struct triangle {

  glm::vec3 x, y, z;
  glm::vec3 centroid;
  bbox my_bbox;

  int triangle_id;

  triangle(const glm::vec3& p_x, const glm::vec3& p_y, const glm::vec3& p_z, int p_index)
  {
    x = p_x;
    y = p_y;
    z = p_z;
    triangle_id = p_index;

    centroid = (x, y, z) / 3.f;
    glm::vec3 min = glm::min(x, y, z);
    glm::vec3 max = glm::max(x, y, z);
    my_bbox = bbox(min, max);
  }

  bbox get_bbox() const { return my_bbox; };
  glm::vec3 get_centroid() const { return centroid; };
};
