
#include "bb.h"
#include <algorithm>
#include <glm/glm.hpp>

bbox::bbox(const glm::vec3& min, const glm::vec3& max)
    : min(min)
    , max(max)
{
  extent = max - min;
}

bbox::bbox(const glm::vec3& p)
    : min(p)
    , max(p)
{
  extent = max - min;
}

void bbox::expandToInclude(const glm::vec3& p)
{
  min = glm::min(min, p);
  max = glm::max(max, p);
  extent = max - min;
}

void bbox::expandToInclude(const bbox& b)
{
  min = glm::min(min, b.min);
  max = glm::max(max, b.max);
  extent = max - min;
}

uint32_t bbox::maxDimension() const
{
  uint32_t result = 0;
  if (extent.y > extent.x) {
    result = 1;
    if (extent.z > extent.y)
      result = 2;
  } else if (extent.z > extent.x)
    result = 2;

  return result;
}

float bbox::surfaceArea() const
{
  return 2.f * (extent.x * extent.z + extent.x * extent.y + extent.y * extent.z);
}

// See: "Real-Time Collision Detection" by Christer Ericson
bool bbox::test_sphere_intersection(const glm::vec3& query_pt, const float radius_squared) const
{
  float sqDist = 0.0f;

  for (int i = 0; i < 3; i++) {
    float v = query_pt[i];
    if (v < min[i]) {
      sqDist += (min[i] - v) * (min[i] - v);
    }
    if (v > max[i]) {
      sqDist += (v - max[i]) * (v - max[i]);
    }
  }

  return (sqDist <= radius_squared);
}