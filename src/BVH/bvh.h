#pragma once

#include "bb.h"
#include <iostream>
#include <stdint.h>
#include <vector>

//! Node descriptor for the flattened tree
struct BVHFlatNode {
  bbox my_bbox;
  uint32_t start, nPrims, rightOffset;
};

//! A Bounding Volume Hierarchy system for fast Ray-triangle intersection tests
class BVH {
  public:
  BVH(std::vector<triangle*>* objects, uint32_t leafSize = 4);
  ~BVH();

  std::vector<int> restrict_search_space(const glm::vec3 query_pt, const float radius_squared) const;
  uint32_t get_num_nodes() const { return nNodes; }
  uint32_t get_size() const { return nNodes * sizeof(BVHFlatNode); }

  private:
  uint32_t nNodes, nLeafs, leafSize;
  std::vector<triangle*>* build_prims;

  void build();
  BVHFlatNode* flatTree;
};