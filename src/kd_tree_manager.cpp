/***********************************************************************
 * Software License Agreement (BSD License)
 *
 * Copyright 2011-2016 Jose Luis Blanco (joseluisblancoc@gmail.com).
 *   All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *************************************************************************/
#include <cstdlib>
#include <ctime>
#include <iostream>

/*
* Sets up the tree: convert the vertices to a pointcloud and build the kd-tree
*/
void kd_tree_manager::init_tree(const std::vector<float>& v_x, const std::vector<float>& v_y, const std::vector<float>& v_z)
{
  cloud.pts.resize(v_x.size());

  for (int i = 0; i < v_x.size(); i++) {
    cloud.pts[i].x = v_x[i];
    cloud.pts[i].y = v_y[i];
    cloud.pts[i].z = v_z[i];
  }

  index.reset(new my_kd_tree_t(3, cloud, KDTreeSingleIndexAdaptorParams(10)));
  index->buildIndex();
}

/*
* Searches for the closest vertex
*/
void kd_tree_manager::search_closest_vertex(const glm::vec3& query_pt_p, int& ret_index_p) const
{
  float query_pt[3] = { query_pt_p[0], query_pt_p[1], query_pt_p[2] };

  // do a knn search where k = 1
  const size_t num_results = 1;
  size_t ret_index;
  float out_dist_sqr;
  nanoflann::KNNResultSet<float> resultSet(num_results);
  resultSet.init(&ret_index, &out_dist_sqr);
  index->findNeighbors(resultSet, &query_pt[0], nanoflann::SearchParams(10));

  ret_index_p = static_cast<int>(ret_index);
}