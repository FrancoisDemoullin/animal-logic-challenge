/*
 * Singleton class for a unit test
 * We only ever want one instance of a unit_test object, having 2 instances
 * would be overkill
 *
 * In all fairness, using a Singleton is overkill here. It adds complexity to
 * the system without adding much value Generally we can assume no 2 instances
 * of a unit test are instantiated by the user of the prgram; yet it is good to
 * be sure.
 */
class unit_test {
  public:
  static unit_test& get_instance()
  {
    static unit_test
        instance; // guaranteed to be destroyed. Instantiated on first use.
    return instance;
  }

  private:
  unit_test() {} // private constructor

  public:
  // make destructors public
  unit_test(unit_test const&) = delete; // copy constructor, delete this isntance
  void operator=(unit_test const&) = delete; // equality operator, delete this instance

  // testing methods
  void run_tests();

  void run_simple_tri(); // checks correctness for simple triangle
  void run_simple_box(); // checks correctness for simple box

  void run_bunny(); // checks parallel speedup and parallel correctness
  void run_happy(); // checks parallel speedup and parallel correctness
};