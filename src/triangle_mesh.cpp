#include "triangle_mesh.h"
#include "glm/ext.hpp" // to_string - for printing and debugging
#include "glm/glm.hpp" // cross + dot
#include "happly.h" // ply loader + writer
#include <assert.h>
#include <limits>

template <class T>
void triangle_mesh<T>::update_hash_map(const std::pair<int, int>& a, const int& key)
{
  if (connectivity_lookup_map.find(key) != connectivity_lookup_map.end()) {
    connectivity_lookup_map[key].insert(a);
  } else {
    std::set<std::pair<int, int>> temp;
    temp.insert(a);
    connectivity_lookup_map[key] = temp;
  }
}

template <class T>
void triangle_mesh<T>::init_hash_map()
{
  for (auto cur_face : vertex_indices) {
    std::pair<int, int> a(cur_face[0], cur_face[1]);
    update_hash_map(a, cur_face[2]);

    std::pair<int, int> b(cur_face[1], cur_face[2]);
    update_hash_map(b, cur_face[0]);

    std::pair<int, int> c(cur_face[0], cur_face[3]);
    update_hash_map(c, cur_face[1]);
  }
}

template <class T>
void triangle_mesh<T>::pre_process_mesh()
{
  init_hash_map();

  kd_tree_instance.init_tree(v_x, v_y, v_z);

  // setup the mesh for the BVH
  // while we are at traversing vertices, prepoulate the mesh's min and max values
  int index = 0;
  bb_min = glm::vec3(v_x[0], v_y[0], v_z[0]);
  bb_max = glm::vec3(v_x[0], v_y[0], v_z[0]);
  for (auto cur_face : vertex_indices) {
    // get the vertices associated with this face
    const glm::vec3 v_0(v_x[cur_face[0]], v_y[cur_face[0]], v_z[cur_face[0]]);
    const glm::vec3 v_1(v_x[cur_face[1]], v_y[cur_face[1]], v_z[cur_face[1]]);
    const glm::vec3 v_2(v_x[cur_face[2]], v_y[cur_face[2]], v_z[cur_face[2]]);
    mesh_for_bvh.push_back(new triangle(v_0, v_1, v_2, index)); // deallocated in destructor of BVH
    index++;

    // populate min and max
    bb_min = glm::min(bb_min, v_0, v_1, v_2);
    bb_max = glm::max(bb_max, v_0, v_1, v_2);
  }

  // build the tree: set number of leaves to 8 triangles
  // deallocated in triangle mesh destructor
  bvh_instance = std::unique_ptr<BVH>(new BVH(&mesh_for_bvh, 8));
}

template <class T>
void triangle_mesh<T>::load_data_ply(const std::string file_name)
{
  // Construct the data object by reading from file
  happly::PLYData mesh(file_name, false);

  // Get mesh-style data from the object
  v_x = mesh.getElement("vertex").getProperty<T>("x");
  v_y = mesh.getElement("vertex").getProperty<T>("y");
  v_z = mesh.getElement("vertex").getProperty<T>("z");
  vertex_indices = mesh.getElement("face").getListProperty<int>("vertex_indices");

  // error checking
  assert(v_x.size() > 0 && v_y.size() > 0 && v_z.size() > 0);
  assert(v_x.size() == v_y.size() && v_y.size() == v_z.size());
  assert(vertex_indices.size() > 0);

  // pre-process the mesh (build hash map, BVH and KD-trees)
  pre_process_mesh();

  std::cout << "======= Pre-process mesh =======" << std::endl;
  std::cout << "Mesh was loaded: Number of triangles: " << vertex_indices.size() << std::endl;
  std::cout << "Extent of mesh: min: " << glm::to_string(bb_min) << " max: " << glm::to_string(bb_max) << std::endl;
  std::cout << std::endl;
  std::cout << "Hash map has been built. Size: " << connectivity_lookup_map.size() / 1024.f << " kB" << std::endl;
  std::cout << "KD tree has been built." << std::endl;
  std::cout << "BVH tree has been built. Number of nodes: " << bvh_instance->get_num_nodes() << " Size: " << bvh_instance->get_size() / 1024.f << " kB" << std::endl;
  std::cout << "================================" << std::endl;
}

/*
* We can be aggressive with compiler keywords
* Passing pointers rather than values and using the restrict keyword + inline the function
*/
float inline dist_squared(glm::vec3 const* __restrict__ a, glm::vec3 const* __restrict__ b)
{
  float dx = a->x - b->x;
  float dy = a->y - b->y;
  float dz = a->z - b->z;
  return dx * dx + dy * dy + dz * dz;
}

/*
* See Real-Time Collision Detection - Christer Ericson 
*/
glm::vec3 closest_pt_on_triangle(const glm::vec3& p, const glm::vec3& a, const glm::vec3& b, const glm::vec3& c)
{
  // Check if P in vertex region outside A
  glm::vec3 ab = b - a;
  glm::vec3 ac = c - a;
  glm::vec3 ap = p - a;
  float d1 = glm::dot(ab, ap);
  float d2 = glm::dot(ac, ap);
  if (d1 <= 0.0f && d2 <= 0.0f)
    return a; // barycentric coordinates (1,0,0)

  // Check if P in vertex region outside B
  glm::vec3 bp = p - b;
  float d3 = glm::dot(ab, bp);
  float d4 = glm::dot(ac, bp);
  if (d3 >= 0.0f && d4 <= d3)
    return b; // barycentric coordinates (0,1,0)

  // Check if P in edge region of AB, if so return projection of P onto AB
  float vc = d1 * d4 - d3 * d2;
  if (vc <= 0.0f && d1 >= 0.0f && d3 <= 0.0f) {
    float v = d1 / (d1 - d3);
    return a + v * ab; // barycentric coordinates (1-v,v,0)
  }

  // Check if P in vertex region outside C
  glm::vec3 cp = p - c;
  float d5 = glm::dot(ab, cp);
  float d6 = glm::dot(ac, cp);
  if (d6 >= 0.0f && d5 <= d6)
    return c; // barycentric coordinates (0,0,1)

  // Check if P in edge region of AC, if so return projection of P onto AC
  float vb = d5 * d2 - d1 * d6;
  if (vb <= 0.0f && d2 >= 0.0f && d6 <= 0.0f) {
    float w = d2 / (d2 - d6);
    return a + w * ac; // barycentric coordinates (1-w,0,w)
  }

  // Check if P in edge region of BC, if so return projection of P onto BC
  float va = d3 * d6 - d5 * d4;
  if (va <= 0.0f && (d4 - d3) >= 0.0f && (d5 - d6) >= 0.0f) {
    float w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
    return b + w * (c - b); // barycentric coordinates (0,1-w,w)
  }

  // P inside face region. Compute Q through its barycentric coordinates (u,v,w)
  float denom = 1.0f / (va + vb + vc);
  float v = vb * denom;
  float w = vc * denom;
  return a + ab * v + ac * w; // =u*a+v*b+w*c,u=va*denom = 1.0f-v-w
}

/*
* Optimized version of the brute force algorithm:
* 1. Potpone divisions until they are necessary
* 2. Use distance squared
* 3. Aggressive compiler keywords + use of -O3 when compiling
* 4. Add early rejection test: if mesh bb and sphere don't intersect at all we are done
*/
template <class T>
void triangle_mesh<T>::closest_pt_on_mesh_brute_force_optimized(const glm::vec3 query_pt,
    const float min_radius,
    glm::vec3& closest_pt,
    float& dist) const
{
  assert(min_radius >= 0);

  // square the radius
  float min_radius_squared = min_radius * min_radius;
  float min_dist = min_radius_squared;
  glm::vec3 min_pt;

  // early rejection test
  if (early_rejection_test(query_pt, min_radius_squared)) {
    dist = -1.f;
    return;
  }

  // iterate over all the faces
  for (auto cur_face : vertex_indices) {
    // get the vertices associated with this face
    glm::vec3 v_0(v_x[cur_face[0]], v_y[cur_face[0]], v_z[cur_face[0]]);
    glm::vec3 v_1(v_x[cur_face[1]], v_y[cur_face[1]], v_z[cur_face[1]]);
    glm::vec3 v_2(v_x[cur_face[2]], v_y[cur_face[2]], v_z[cur_face[2]]);

    glm::vec3 intersection_pt = closest_pt_on_triangle(query_pt, v_0, v_1, v_2);

    float intersection_dist = dist_squared(
        &intersection_pt,
        &query_pt);

    if (intersection_dist <= min_radius_squared && intersection_dist < min_dist) {
      min_dist = intersection_dist;
      min_pt = intersection_pt;
    }
  }

  // return the result via the passed variables closest_pt and dist
  if (min_dist < min_radius_squared) {
    assert(min_dist >= 0); // sanity check
    closest_pt = min_pt;
    dist = std::sqrt(min_dist); // take the sqrt here! Only once instead of at every single triangle
  } else {
    dist = -1.f;
  }
}

/*
* We reduced the "closest point on mesh" problem to a different problem:
* 1. Find closest vertex, 
* 2. Then use brute force and all the triangles that contain the closest vertex
* 
* use the hash map that has been built during the mesh's pre_processing for faster lookups
*/
template <class T>
void triangle_mesh<T>::closest_pt_on_mesh_vert_triangle_optimization(const glm::vec3 query_pt,
    const float min_radius,
    glm::vec3& closest_pt,
    float& dist) const
{
  assert(min_radius >= 0);

  // square the radius
  float min_radius_squared = min_radius * min_radius;
  float min_dist = min_radius_squared;
  glm::vec3 min_pt;
  int closest_vertex_id = -1;

  // early rejection test
  if (early_rejection_test(query_pt, min_radius_squared)) {
    dist = -1.f;
    return;
  }

  // iterate over all the faces
  for (unsigned int i = 0; i < vertex_indices.size(); i++) {
    auto cur_face = vertex_indices[i];

    // get the vertices associated with this face
    for (int j = 0; j < 3; j++) {
      // compare only vertex distances
      glm::vec3 cur_vertex(v_x[cur_face[j]], v_y[cur_face[j]], v_z[cur_face[j]]);

      float intersection_dist = dist_squared(
          &cur_vertex,
          &query_pt);

      if (intersection_dist < min_dist) {
        min_dist = intersection_dist;
        closest_vertex_id = cur_face[j];
      }
    }
  }

  // early exit condiditon
  if (closest_vertex_id < 0) {
    dist = -1.f;
    return;
  }

  // only compute closest triangle distance on triangles containing closest vertex
  // do a lookup in the hasmap:
  auto triangle_shortlist = connectivity_lookup_map.find(closest_vertex_id);
  assert(triangle_shortlist != connectivity_lookup_map.end()); // sanity check

  glm::vec3 v_0(v_x[closest_vertex_id], v_y[closest_vertex_id], v_z[closest_vertex_id]);

  for (auto cur_pair : triangle_shortlist->second) {
    // get the vertices associated with this face
    glm::vec3 v_1(v_x[cur_pair.first], v_y[cur_pair.first], v_z[cur_pair.first]);
    glm::vec3 v_2(v_x[cur_pair.second], v_y[cur_pair.second], v_z[cur_pair.second]);

    glm::vec3 intersection_pt = closest_pt_on_triangle(query_pt, v_0, v_1, v_2);

    float intersection_dist = dist_squared(
        &intersection_pt,
        &query_pt);

    if (intersection_dist <= min_radius_squared && intersection_dist <= min_dist) {
      min_dist = intersection_dist;
      min_pt = intersection_pt;
    }
  }

  // return the result via the passed variables closest_pt and dist
  if (min_dist < min_radius_squared) {
    assert(min_dist >= 0); // sanity check
    closest_pt = min_pt;
    dist = std::sqrt(min_dist); // take the sqrt here! Only once instead of at every single triangle
  } else {
    dist = -1.f;
  }
}

/*
* Use a kd tree to compute the closest neighbor vertex
* Then use brute force on the triangles that contain the closest vertex
*/
template <class T>
void triangle_mesh<T>::closest_pt_kd_tree(const glm::vec3 query_pt,
    const float min_radius,
    glm::vec3& closest_pt,
    float& dist) const
{
  float min_radius_squared = min_radius * min_radius;
  float min_dist = min_radius_squared;
  int closest_vertex_id;
  glm::vec3 min_pt;

  if (early_rejection_test(query_pt, min_radius_squared)) {
    dist = -1.f;
    return;
  }

  kd_tree_instance.search_closest_vertex(query_pt, closest_vertex_id);
  glm::vec3 v_0(v_x[closest_vertex_id], v_y[closest_vertex_id], v_z[closest_vertex_id]);

  // early rejection:
  if (dist_squared(&v_0, &query_pt) > min_radius_squared) {
    dist = -1.f;
    return;
  }

  // only compute closest triangle distance on triangles containing closest vertex
  // do a lookup in the hasmap:
  auto triangle_shortlist = connectivity_lookup_map.find(closest_vertex_id);
  assert(triangle_shortlist != connectivity_lookup_map.end()); // sanity check

  for (auto cur_pair : triangle_shortlist->second) {
    // get the vertices associated with this face
    glm::vec3 v_1(v_x[cur_pair.first], v_y[cur_pair.first], v_z[cur_pair.first]);
    glm::vec3 v_2(v_x[cur_pair.second], v_y[cur_pair.second], v_z[cur_pair.second]);

    glm::vec3 intersection_pt = closest_pt_on_triangle(query_pt, v_0, v_1, v_2);

    float intersection_dist = dist_squared(
        &intersection_pt,
        &query_pt);

    if (intersection_dist <= min_radius_squared && intersection_dist <= min_dist) {
      min_dist = intersection_dist;
      min_pt = intersection_pt;
    }
  }

  // return the result via the passed variables closest_pt and dist
  if (min_dist < min_radius_squared) {
    assert(min_dist >= 0); // sanity check
    closest_pt = min_pt;
    dist = std::sqrt(min_dist); // take the sqrt here! Only once instead of at every single triangle
  } else {
    dist = -1.f;
  }
}

/*
* Use a BVH tree to prune the search space
* If a lot of search-space can be pruned this is highly efficient
* If no search-space can be pruned this is slower than brute force
*/
template <class T>
void triangle_mesh<T>::closest_pt_bvh(const glm::vec3 query_pt,
    const float min_radius,
    glm::vec3& closest_pt,
    float& dist) const
{
  float min_radius_squared = min_radius * min_radius;
  float min_dist = min_radius_squared;
  int closest_vertex_id;
  glm::vec3 min_pt;

  std::vector<int> shortlist = bvh_instance->restrict_search_space(query_pt, min_radius_squared);
  std::cout << "shortlist.size(): " << shortlist.size() << " - pruning percentage: "
            << 100.f * (1.f - (static_cast<float>(shortlist.size()) / static_cast<float>(vertex_indices.size()))) << "%" << std::endl;

  // iterate over all the faces
  for (auto index : shortlist) {

    auto cur_face = vertex_indices[index];

    // get the vertices associated with this face
    glm::vec3 v_0(v_x[cur_face[0]], v_y[cur_face[0]], v_z[cur_face[0]]);
    glm::vec3 v_1(v_x[cur_face[1]], v_y[cur_face[1]], v_z[cur_face[1]]);
    glm::vec3 v_2(v_x[cur_face[2]], v_y[cur_face[2]], v_z[cur_face[2]]);

    glm::vec3 intersection_pt = closest_pt_on_triangle(query_pt, v_0, v_1, v_2);

    float intersection_dist = dist_squared(
        &intersection_pt,
        &query_pt);

    if (intersection_dist <= min_radius_squared && intersection_dist < min_dist) {
      min_dist = intersection_dist;
      min_pt = intersection_pt;
    }
  }

  // return the result via the passed variables closest_pt and dist
  if (min_dist < min_radius_squared) {
    assert(min_dist >= 0); // sanity check
    closest_pt = min_pt;
    dist = std::sqrt(min_dist); // take the sqrt here! Only once instead of at every single triangle
  } else {
    dist = -1.f;
  }
}

/*
 *
 *  DEPRECATED CODE
 *
 *
 */

/*
 * Utility function: get the closest point to a query_pt on a line segment
 */
glm::vec3 closes_pt_on_segment(const glm::vec3 v_0,
    const glm::vec3 v_1,
    const glm::vec3 query_pt)
{
  glm::vec3 v = v_1 - v_0;
  glm::vec3 w = query_pt - v_0;

  float c1 = glm::dot(w, v);
  if (c1 < 0) {
    return v_0; // vertex is closest
  }

  float c2 = glm::dot(v, v);
  if (c2 <= c1) {
    return v_1; // vertex is closest
  }

  // point on the line between v_0 and v_1 is closest
  float b = c1 / c2;
  glm::vec3 result = v_0 + v * b;
  return result;
}

/*
 * The algorithm I use to compute the distance between a point and a triangle is
 * described in: W. Heidrich, Journal of Graphics, GPU, and Game Tools,Volume
 * 10, Issue 3, 2005 Computing the Barycentric Coordinates of a Projected Point
 * Link to paper without paywall:
 * https://vccimaging.org/Publications/Heidrich2005CBP/Heidrich2005CBP.pdf
 *
 * Note: the author of this paper is affiliated with UBC. Go UBC!! :)
 */
template <class T>
void triangle_mesh<T>::closest_pt_on_mesh_brute_force(const glm::vec3 query_pt,
    const float min_radius,
    glm::vec3& closest_pt,
    float& dist) const
{
  assert(min_radius >= 0);

  float min_dist = std::numeric_limits<float>::max();
  glm::vec3 min_pt;

  // iterate over all the faces
  for (auto cur_face : vertex_indices) {
    // get the vertices associated with this face
    glm::vec3 v_0(v_x[cur_face[0]], v_y[cur_face[0]], v_z[cur_face[0]]);
    glm::vec3 v_1(v_x[cur_face[1]], v_y[cur_face[1]], v_z[cur_face[1]]);
    glm::vec3 v_2(v_x[cur_face[2]], v_y[cur_face[2]], v_z[cur_face[2]]);

    // get vectors from vertices
    glm::vec3 u = v_1 - v_0;
    glm::vec3 v = v_2 - v_0;
    glm::vec3 w = query_pt - v_0; // helper vector

    // compute normal to triangle - this normal is not normalized
    glm::vec3 n = glm::cross(u, v);

    // get barycentrics of projected point query_pt', projected into the plane
    // of current triangle
    float gamma = (glm::dot(glm::cross(u, w), n)) / glm::dot(n, n);
    float beta = (glm::dot(glm::cross(w, v), n)) / glm::dot(n, n);
    float alpha = 1.f - beta - gamma;

    glm::vec3 intersection_pt;

    // check if the projected point is inside triangle
    if (alpha >= 0.f && alpha <= 1.f && beta >= 0.f && beta <= 1.f && gamma >= 0.f && gamma <= 1.f) {
      // yes! point is within the triangle, compute it using barycentrics
      intersection_pt = alpha * v_0 + beta * v_1 + gamma * v_2;
    } else if (alpha >= 0 && beta >= 0 && gamma < 0) {
      // region 1
      // projected point is outside of triangle, closes point lies on line
      // segment [v_0, v_1]
      intersection_pt = closes_pt_on_segment(v_0, v_1, query_pt);
    } else if (alpha < 0 && beta >= 0 && gamma < 0) {
      // region 2
      if (gamma <= alpha) {
        // region 2a
        intersection_pt = closes_pt_on_segment(v_0, v_1, query_pt);
      } else {
        intersection_pt = closes_pt_on_segment(v_1, v_2, query_pt);
      }
    } else if (alpha < 0 && beta >= 0 && gamma >= 0) {
      // region 3
      // projected point is outside of triangle, closes point lies on line
      // segment [v_1, v_2]
      intersection_pt = closes_pt_on_segment(v_1, v_2, query_pt);
    } else if (alpha < 0 && beta < 0 && gamma >= 0) {
      // region 4
      if (alpha <= beta) {
        intersection_pt = closes_pt_on_segment(v_1, v_2, query_pt);
      } else {
        intersection_pt = closes_pt_on_segment(v_0, v_2, query_pt);
      }
    } else if (alpha >= 0 && beta < 0 && gamma >= 0) {
      // region 5
      // projected point is outside of triangle, closes point lies on line
      // segment [v_0, v_2]
      intersection_pt = closes_pt_on_segment(v_0, v_2, query_pt);
    } else {
      // region 6
      if (beta >= gamma) {
        intersection_pt = closes_pt_on_segment(v_0, v_2, query_pt);
      } else {
        intersection_pt = closes_pt_on_segment(v_0, v_1, query_pt);
      }
    }

    float intersection_dist = glm::distance(
        intersection_pt,
        query_pt); // TODO: replace distance by distance_squared to avoid sqrt!!
    if (intersection_dist <= min_radius && intersection_dist < min_dist) {
      min_dist = intersection_dist;
      min_pt = intersection_pt;
    }
  }

  // return the result via the passed variables closest_pt and dist
  if (min_dist < std::numeric_limits<float>::max()) {
    assert(min_dist >= 0); // sanity check
    closest_pt = min_pt;
    dist = min_dist;
  } else {
    dist = -1.f;
  }
}

/*
 * CPU paralellized verision of the brute force algorithm
 * This function is analoguous to closest_pt_on_mesh_brute_force
 * For detailed comments refer to closest_pt_on_mesh_brute_force
 *
 * I added 2 OpenMP pragmas: one for the paralellized for loop
 * and another for the critical section
 */
template <class T>
void triangle_mesh<T>::closest_pt_on_mesh_brute_force_cpu_parallel(
    const glm::vec3 query_pt,
    const float min_radius,
    glm::vec3& closest_pt,
    float& dist) const
{
  assert(min_radius >= 0);

  float min_dist = std::numeric_limits<float>::max();
  glm::vec3 min_pt;

#pragma omp parallel for schedule(dynamic)
  for (unsigned int i = 0; i < vertex_indices.size();
       i++) // OpenMP does not support C++11 syntax for for loops
  {
    auto cur_face = vertex_indices[i];
    glm::vec3 v_0(v_x[cur_face[0]], v_y[cur_face[0]], v_z[cur_face[0]]);
    glm::vec3 v_1(v_x[cur_face[1]], v_y[cur_face[1]], v_z[cur_face[1]]);
    glm::vec3 v_2(v_x[cur_face[2]], v_y[cur_face[2]], v_z[cur_face[2]]);
    glm::vec3 u = v_1 - v_0;
    glm::vec3 v = v_2 - v_0;
    glm::vec3 w = query_pt - v_0;
    glm::vec3 n = glm::cross(u, v);
    float gamma = (glm::dot(glm::cross(u, w), n)) / glm::dot(n, n);
    float beta = (glm::dot(glm::cross(w, v), n)) / glm::dot(n, n);
    float alpha = 1.f - beta - gamma;
    glm::vec3 intersection_pt;

    if (alpha >= 0.f && alpha <= 1.f && beta >= 0.f && beta <= 1.f && gamma >= 0.f && gamma <= 1.f) {
      intersection_pt = alpha * v_0 + beta * v_1 + gamma * v_2;
    } else if (alpha >= 0 && beta >= 0 && gamma < 0) {
      intersection_pt = closes_pt_on_segment(v_0, v_1, query_pt);
    } else if (alpha < 0 && beta >= 0 && gamma < 0) {
      if (gamma <= alpha) {
        intersection_pt = closes_pt_on_segment(v_0, v_1, query_pt);
      } else {
        intersection_pt = closes_pt_on_segment(v_1, v_2, query_pt);
      }
    } else if (alpha < 0 && beta >= 0 && gamma >= 0) {
      intersection_pt = closes_pt_on_segment(v_1, v_2, query_pt);
    } else if (alpha < 0 && beta < 0 && gamma >= 0) {
      if (alpha <= beta) {
        intersection_pt = closes_pt_on_segment(v_1, v_2, query_pt);
      } else {
        intersection_pt = closes_pt_on_segment(v_0, v_2, query_pt);
      }
    } else if (alpha >= 0 && beta < 0 && gamma >= 0) {
      intersection_pt = closes_pt_on_segment(v_0, v_2, query_pt);
    } else {
      if (beta >= gamma) {
        intersection_pt = closes_pt_on_segment(v_0, v_2, query_pt);
      } else {
        intersection_pt = closes_pt_on_segment(v_0, v_1, query_pt);
      }
    }

    float intersection_dist = glm::distance(
        intersection_pt,
        query_pt); // TODO: replace distance by dot to avoid sqrt!!

#pragma omp critical
    if (intersection_dist <= min_radius && intersection_dist < min_dist) {
      min_dist = intersection_dist;
      min_pt = intersection_pt;
    }
  }

  if (min_dist < std::numeric_limits<float>::max()) {
    assert(min_dist >= 0); // sanity check
    closest_pt = min_pt;
    dist = min_dist;
  } else {
    dist = -1.f;
  }
}

// forward declaring the cuda_entry_point function
void cuda_entry_point(const float* x_data,
    const float* y_data,
    const float* z_data,
    const unsigned int* connectivity,
    const unsigned int num_verts,
    const unsigned int num_connectivity,
    const float q_pt_x,
    const float q_pt_y,
    const float q_pt_z,
    const float radius,
    float& closest_pt_x, // glm and nvcc don't play nicely together, do this instead
    float& closest_pt_y, // we are passing the coordinates of query_pt rather than a glm::vec
    float& closest_pt_z,
    float& dist);

/*
 * Host code to setup the CUDA kernel
 */
template <class T>
void triangle_mesh<T>::closest_pt_on_mesh_brute_force_gpu(
    const glm::vec3 query_pt,
    const float min_radius,
    glm::vec3& closest_pt,
    float& dist) const
{
  /*
   * pre-process the data for CUDA, once per mesh only
   */
  const float* x_data = v_x.data();
  const float* y_data = v_y.data();
  const float* z_data = v_z.data();

  // flatten connectivity vector
  unsigned int* connectivity = (unsigned int*)malloc(3 * vertex_indices.size() * sizeof(unsigned int));
  for (unsigned int i = 0; i < vertex_indices.size(); i++) {
    connectivity[3 * i] = vertex_indices[i][0];
    connectivity[3 * i + 1] = vertex_indices[i][1];
    connectivity[3 * i + 2] = vertex_indices[i][2];
  }

  float closest_pt_x, closest_pt_y, closest_pt_z;
  cuda_entry_point(x_data, y_data, z_data, connectivity, v_x.size(),
      vertex_indices.size(), query_pt[0], query_pt[1],
      query_pt[2], min_radius,
      closest_pt_x, closest_pt_y, closest_pt_z, dist);

  // update the return value
  closest_pt = glm::vec3(closest_pt_x, closest_pt_y, closest_pt_z); // convert back to glm::vec
  free(connectivity);
}