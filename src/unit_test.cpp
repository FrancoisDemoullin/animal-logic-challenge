
#include "unit_test.h"
#include "glm/gtx/string_cast.hpp" // to_string - for printing and debugging
#include "triangle_mesh.h"
#include <chrono> // timing
#include <glm/vec3.hpp>
#include <iomanip> // precision when printing

/*
* use this epsilon value instead of floating point equality
* crucial when comparing GPU results!
*/
#define EPSILON 0.0001

void unit_test::run_tests()
{
  run_simple_tri();
  run_simple_box();
  run_bunny();
  run_happy();
  std::cout << "all unit tests passed" << std::endl;
}

/*
 * Unit Tests for the simple triangle mesh -> 1 triangle, 3 vertices
 */
void unit_test::run_simple_tri()
{
  triangle_mesh<float> simple_tri_mesh; // pass float as the template parameter
      // - interpret vertices as floats
  simple_tri_mesh.load_data_ply("./data/simple_tri.ply");

  glm::vec3 point, result_pt;
  float result_dist;

  // region 1:
  point = glm::vec3(-0.5f, 0.5f, 0.f);
  simple_tri_mesh.closest_pt_on_mesh_brute_force_optimized(point, 10.f, result_pt,
      result_dist);
  assert(result_pt == glm::vec3(0.f, 0.5f, 0.f));
  assert(result_dist == 0.5);

  // region 2a:
  point = glm::vec3(-0.99f, 2.f, 0.f);
  simple_tri_mesh.closest_pt_on_mesh_brute_force_optimized(point, 10.f, result_pt,
      result_dist);
  assert(result_pt == glm::vec3(0.f, 1.f, 0.f));

  // region 2b:
  point = glm::vec3(-0.01f, 2.f, 0.f);
  simple_tri_mesh.closest_pt_on_mesh_brute_force_optimized(point, 10.f, result_pt,
      result_dist);
  assert(result_pt == glm::vec3(0.f, 1.f, 0.f));

  // region 3:
  point = glm::vec3(0.75f, 0.75f, 0.f);
  simple_tri_mesh.closest_pt_on_mesh_brute_force_optimized(point, 10.f, result_pt,
      result_dist);
  assert(result_pt == glm::vec3(0.5f, 0.5f, 0.f));

  // region 4a:
  point = glm::vec3(2.f, -0.01f, 0.f);
  simple_tri_mesh.closest_pt_on_mesh_brute_force_optimized(point, 10.f, result_pt,
      result_dist);
  assert(result_pt == glm::vec3(1.f, 0.f, 0.f));

  // region 4b:
  point = glm::vec3(2.f, -0.99f, 0.f);
  simple_tri_mesh.closest_pt_on_mesh_brute_force_optimized(point, 10.f, result_pt,
      result_dist);
  assert(result_pt == glm::vec3(1.f, 0.f, 0.f));

  // region 5:
  point = glm::vec3(0.5f, -0.5f, 0.f);
  simple_tri_mesh.closest_pt_on_mesh_brute_force_optimized(point, 10.f, result_pt,
      result_dist);
  assert(result_pt == glm::vec3(0.5f, 0.f, 0.f));
  assert(result_dist == 0.5);

  // region 6a:
  point = glm::vec3(-2.f, -0.01f, 0.f);
  simple_tri_mesh.closest_pt_on_mesh_brute_force_optimized(point, 10.f, result_pt,
      result_dist);
  assert(result_pt == glm::vec3(0.f, 0.f, 0.f));

  // region 6b:
  point = glm::vec3(-2.f, -0.99f, 0.f);
  simple_tri_mesh.closest_pt_on_mesh_brute_force_optimized(point, 10.f, result_pt,
      result_dist);
  assert(result_pt == glm::vec3(0.f, 0.f, 0.f));
}

/*
 * Unit Tests for the simple cube mesh -> 12 triangles, 8 vertices
 */
void unit_test::run_simple_box()
{
  // load the cube mesh
  triangle_mesh<float> cur_mesh; // pass float as the template parameter -
      // interpret vertices as floats
  cur_mesh.load_data_ply("./data/cube.ply");

  glm::vec3 point, result_pt;
  float result_dist;

  // Test 1: the closest point is a vertex
  point = glm::vec3(1.f, 1.f, 1.f);
  cur_mesh.closest_pt_on_mesh_brute_force_optimized(point, 10.f, result_pt, result_dist);
  assert(result_pt == point);
  assert(result_dist == 0.f);

  // Test 2: the closest point is on the egde of a triangle
  point = glm::vec3(2.f, 0.f, 0.f);
  cur_mesh.closest_pt_on_mesh_brute_force_optimized(point, 10.f, result_pt, result_dist);
  assert(result_pt == glm::vec3(1.f, 0.f, 0.f));
  assert(result_dist == 1.f);

  // Test 3: there is no closest point within the radius
  point = glm::vec3(3.f, 0.f, 0.f);
  cur_mesh.closest_pt_on_mesh_brute_force_optimized(point, 1.f, result_pt, result_dist);
  assert(result_dist < 0.f);

  // Test 4: the closest point is inside a triangle (not a vertex nor an edge)
  point = glm::vec3(0.5f, 0.5f, 2.f);
  cur_mesh.closest_pt_on_mesh_brute_force_optimized(point, 10.f, result_pt, result_dist);
  assert(result_pt == glm::vec3(0.5f, 0.5f, 1.f));
  assert(result_dist == 1.f);

  // Test 5: there are multiple closest points
  point = glm::vec3(0.f, 0.f, 0.0f);
  cur_mesh.closest_pt_on_mesh_brute_force_optimized(point, 10.f, result_pt, result_dist);
  assert(result_dist == 1.f);
}

/*
 * Unit Tests for the bunny mesh
 *
 */
void unit_test::run_bunny()
{
  triangle_mesh<float> bunny;
  bunny.load_data_ply("./data/bunny.ply");

  glm::vec3 closest_point, closest_vertex_point, closest_kd_point, closest_bvh_point;
  float closest_dist, closest_vertex_dist, closest_kd_dist, closest_bvh_dist;

  std::vector<glm::vec3> pt_vec = {glm::vec3(2.0f, 1.0f, 3.0f), glm::vec3(0.5f, 0.5f, 0.8f), glm::vec3(4.0f, 4.0f, 4.0f)};
  float radius = 10.f;

  for (glm::vec3 point : pt_vec) {
    
    auto t_start = std::chrono::high_resolution_clock::now();
    bunny.closest_pt_on_mesh_brute_force_optimized(point, radius, closest_point, closest_dist);
    auto t_end = std::chrono::high_resolution_clock::now();
    std::cout
        << "cpu brute force: "
        << std::chrono::duration<double, std::milli>(t_end - t_start).count()
        << std::endl;

    t_start = std::chrono::high_resolution_clock::now();
    bunny.closest_pt_on_mesh_vert_triangle_optimization(point, radius, closest_vertex_point, closest_vertex_dist);
    t_end = std::chrono::high_resolution_clock::now();
    std::cout
        << "cpu vertex: "
        << std::chrono::duration<double, std::milli>(t_end - t_start).count()
        << std::endl;
    assert(abs(closest_vertex_dist - closest_dist) < EPSILON);

    t_start = std::chrono::high_resolution_clock::now();
    bunny.closest_pt_kd_tree(point, radius, closest_kd_point, closest_kd_dist);
    t_end = std::chrono::high_resolution_clock::now();
    std::cout
        << "kd tree: "
        << std::chrono::duration<double, std::milli>(t_end - t_start).count()
        << std::endl;
    assert(abs(closest_vertex_dist - closest_kd_dist) < EPSILON);

    t_start = std::chrono::high_resolution_clock::now();
    bunny.closest_pt_bvh(point, radius, closest_bvh_point, closest_bvh_dist);
    t_end = std::chrono::high_resolution_clock::now();
    std::cout
        << "bvh tree: "
        << std::chrono::duration<double, std::milli>(t_end - t_start).count()
        << std::endl;
    assert(abs(closest_vertex_dist - closest_bvh_dist) < EPSILON);
  }
};

/*
 * Unit Tests for the happy mesh
 */
void unit_test::run_happy()
{
  triangle_mesh<float> happy;
  happy.load_data_ply("./data/happy_vrip.ply");

  glm::vec3 point, result_pt, result_pt_cpu_parallel, result_pt_gpu_parallel, result_pt_serial_optimized;
  glm::vec3 closest_vertex_point, closest_kd_point, closest_bvh_point;
  float result_dist, result_dist_cpu_parallel, result_dist_gpu_parallel, result_dist_serial_optimized;
  float closest_vertex_dist, closest_kd_dist, closest_bvh_dist;

  /*
  * I am testing how much pruning the BVH has to do to beat brute force and the kd tree
  */
  point = glm::vec3(2.0f, 1.0f, 3.0f);
  std::vector<float> radius_vec = {
    3.8f, // no pruning of the mesh
    3.75f, // ~8% pruning
    3.7f, // ~73% pruning
    3.69f, // ~86.5% pruning
    3.685f, // 93.4% pruning
    3.68f, // ~98.7% pruning
    3.6f // entire mesh is pruned -> early rejection kicks in for kd tree and brute force
  };

  for (float radius : radius_vec) {
    auto t_start = std::chrono::high_resolution_clock::now();
    happy.closest_pt_on_mesh_vert_triangle_optimization(point, radius, closest_vertex_point, closest_vertex_dist);
    auto t_end = std::chrono::high_resolution_clock::now();
    std::cout
        << "cpu vertex: "
        << std::chrono::duration<double, std::milli>(t_end - t_start).count()
        << std::endl;
    //std::cout << "closest vert: " << glm::to_string(closest_vertex_point) << std::endl;
    //std::cout << "dist: " << closest_vertex_dist << std::endl;

    t_start = std::chrono::high_resolution_clock::now();
    happy.closest_pt_kd_tree(point, radius, closest_kd_point, closest_kd_dist);
    t_end = std::chrono::high_resolution_clock::now();
    std::cout
        << "kd tree: "
        << std::chrono::duration<double, std::milli>(t_end - t_start).count()
        << std::endl;

    //std::cout << "closest vert: " << glm::to_string(closest_kd_point) << std::endl;
    //std::cout << "dist: " << closest_kd_dist << std::endl;
    assert(abs(closest_vertex_dist - closest_kd_dist) < EPSILON);

    t_start = std::chrono::high_resolution_clock::now();
    happy.closest_pt_bvh(point, radius, closest_bvh_point, closest_bvh_dist);
    t_end = std::chrono::high_resolution_clock::now();
    std::cout
        << "bvh tree: "
        << std::chrono::duration<double, std::milli>(t_end - t_start).count()
        << std::endl;

    //std::cout << "closest vert: " << glm::to_string(closest_bvh_point) << std::endl;
    //std::cout << "dist: " << closest_bvh_dist << std::endl;
    assert(abs(closest_vertex_dist - closest_bvh_dist) < EPSILON);
  }


  /*
  * Testing some more points
  */
  std::vector<glm::vec3> point_vec = {glm::vec3(1.0f, 1.0f, 5.0f), glm::vec3(2.0f, 1.0f, -2.0f)};
  float radius = 10.f;

  for (auto cur_point : point_vec) {
    point = cur_point;
    
    auto t_start = std::chrono::high_resolution_clock::now();
    happy.closest_pt_on_mesh_vert_triangle_optimization(point, radius, closest_vertex_point, closest_vertex_dist);
    auto t_end = std::chrono::high_resolution_clock::now();
    std::cout
        << "cpu vertex: "
        << std::chrono::duration<double, std::milli>(t_end - t_start).count()
        << std::endl;
    //std::cout << "closest vert: " << glm::to_string(closest_vertex_point) << std::endl;
    //std::cout << "dist: " << closest_vertex_dist << std::endl;

    t_start = std::chrono::high_resolution_clock::now();
    happy.closest_pt_kd_tree(point, radius, closest_kd_point, closest_kd_dist);
    t_end = std::chrono::high_resolution_clock::now();
    std::cout
        << "kd tree: "
        << std::chrono::duration<double, std::milli>(t_end - t_start).count()
        << std::endl;

    //std::cout << "closest vert: " << glm::to_string(closest_kd_point) << std::endl;
    //std::cout << "dist: " << closest_kd_dist << std::endl;
    assert(abs(closest_vertex_dist - closest_kd_dist) < EPSILON);

    t_start = std::chrono::high_resolution_clock::now();
    happy.closest_pt_bvh(point, radius, closest_bvh_point, closest_bvh_dist);
    t_end = std::chrono::high_resolution_clock::now();
    std::cout
        << "bvh tree: "
        << std::chrono::duration<double, std::milli>(t_end - t_start).count()
        << std::endl;

    //std::cout << "closest vert: " << glm::to_string(closest_bvh_point) << std::endl;
    //std::cout << "dist: " << closest_bvh_dist << std::endl;
    assert(abs(closest_vertex_dist - closest_bvh_dist) < EPSILON);
  }
}