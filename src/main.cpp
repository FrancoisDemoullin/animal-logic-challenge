#include "unit_test.h"
#include <iostream>

/*
 * Entry point - keep main file clean
 */
int main()
{

  /*
 * Run Unit Tests
 */
  unit_test::get_instance().run_tests();
  std::cout << "Done!" << std::endl;
}